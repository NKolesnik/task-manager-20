package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task updateById(String userId, String id, String name, String description);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusById(String userId, String id, Status status);

}
