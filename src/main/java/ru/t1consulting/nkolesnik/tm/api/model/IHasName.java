package ru.t1consulting.nkolesnik.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
